//
//  AppDelegate.h
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

