//
//  ViewController.h
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sort;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *search;

@end

