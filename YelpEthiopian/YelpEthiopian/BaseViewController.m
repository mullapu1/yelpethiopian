//
//  ViewController.m
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import "BaseViewController.h"
#import "DataManager.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.sort setAction:@selector(showActionSheet)];
    [self.search setAction:@selector(performSearchTask)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


/**
 *  Allows the user to perform a search term
 */
- (void) performSearchTask {
    UIAlertController * ac= [UIAlertController alertControllerWithTitle:@"Search for a business" message:@"Please enter a search term" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* search = [UIAlertAction actionWithTitle:@"Search" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UITextField *textfield = ac.textFields.firstObject;
        [[DataManager sharedManager] updateSearchTerm:textfield.text];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"searchTermUpdated" object:nil];
    }];
    [ac addAction:search];

    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [ac dismissViewControllerAnimated:YES completion:nil];
    }];
    [ac addAction:cancel];
    
    [ac addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.tag = 100;
        textField.placeholder = @"Ethiopian";
    }];
    
    [self presentViewController:ac animated:YES completion:nil];
    
}

/**
 *  Allows the user to perform sort function
 */
- (void) showActionSheet {
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Sort By:" message:@"Please choose a sorting method" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *bestMatched = [UIAlertAction actionWithTitle:@"BestMatched" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       [[DataManager sharedManager] chooseSortType:@"0"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sortValueUpdated" object:nil];
    }];
    
    [ac addAction:bestMatched];
    
    UIAlertAction *distanceAction = [UIAlertAction actionWithTitle:@"Distance" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[DataManager sharedManager] chooseSortType:@"1"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sortValueUpdated" object:nil];
    }];
    
    [ac addAction:distanceAction];
    
    UIAlertAction *highestRated = [UIAlertAction actionWithTitle:@"Highest Rated" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[DataManager sharedManager] chooseSortType:@"2"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sortValueUpdated" object:nil];
    }];
    
    [ac addAction:highestRated];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [ac addAction:cancelAction];
    
    [self presentViewController:ac animated:YES completion:nil];
}
@end
