//
//  DataManager.h
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Manages the flow of the Data between Model and ViewController
 */
@interface DataManager : NSObject

@property (nonatomic, strong) NSArray* businesses;
@property (nonatomic, strong) NSString *sortType;
@property (nonatomic, strong) NSString *searchTerm;

/**
 *  Stores the user selected search term
 *
 *  @param searchTerm <#searchTerm description#>
 */
- (void) updateSearchTerm:(NSString*)searchTerm;

/**
 *  Stores the user selected sort method
 *
 *  @param sort <#sort description#>
 */
- (void) chooseSortType:(NSString*)sort;

/**
 *  Creates a Singleton Instance
 *
 *  @return Self
 */
+ (DataManager*) sharedManager;


/**
 *  Downloads details of a given business
 *
 *  @param businessID        Business ID required to download information for
 *  @param completionHandler contains the dictionary or an error object
 */
- (void) getBusinessDeatailsForID:(NSString*)businessID completionHandler:(void (^)(NSDictionary *businessJSON, NSError *error))completionHandler;

/**
 *  Gets a list of restaurants
 *
 *  @param completionHandler Indicates a whether the fetch is sucess or not
 */
- (void) getListOfRestaurantsWithCompletionHandler:(void (^)(BOOL finished))completionHandler;

@end
