//
//  DataManager.m
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import "DataManager.h"
#import <TDOAuth/TDOAuth.h>
#import "Business.h"

@implementation DataManager

static NSString * const kConsumerKey       = @"SPcwD8VdPD3-HzXVqpWa5Q";
static NSString * const kConsumerSecret    = @"-nlo1JUclMJ0U9t7x5srb2SAw8A";
static NSString * const kToken             = @"9BsZT9wbm-t76lW6RckfhP35eeZ4YjAB";
static NSString * const kTokenSecret       = @"9CaxgR9GYDuAOUmTS6TnRYyXBsA";

static NSString * const kAPIHost           = @"api.yelp.com";
static NSString * const kSearchPath        = @"/v2/search/";
static NSString * const kBusinessPath      = @"/v2/business/";
static NSString * const kSearchLimit       = @"10";
static NSString * const kCountryCode       = @"CA";
static NSString * const kLocation          = @"Toronto";

/**
 *  Creates Singleton instance
 *
 *  @return self
 */
+ (DataManager*) sharedManager {
    static DataManager * _sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[self alloc] init];

    });
    
    return _sharedInstance;
}

- (id) init {
    self.businesses = [NSArray array];
    self.sortType = @"0";
    self.searchTerm = @"Ethiopian";
    return  self;
}

- (void) chooseSortType:(NSString*)sort {
    self.sortType = sort;
}

- (void) updateSearchTerm:(NSString*)searchTerm {
    self.searchTerm = searchTerm;
}

/**
 *  Generate Search Parameters
 *
 *  @param term Required Search Term
 *  @param sort Sorting Type - Default : Sort by rating
 *
 *  @return NSDictionary of search parameters for the GET request
 */
- (NSDictionary*)generateSearchParameters:(NSString*)term sortBy:(NSString*)sort {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:term forKey:@"term"];
    [params setObject:kLocation forKey:@"location"];
    [params setObject:kSearchLimit forKey:@"limit"];
    [params setObject:kCountryCode forKey:@"cc"];
    
    [params setObject:self.sortType forKey:@"sort"];
    return params;
}

- (NSDictionary*)generateBusinessParams:(NSString*)businessID {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:kCountryCode forKey:@"cc"];
    
    return params;
}

/**
 *  Creates a Search Request
 *
 *  @param searchTerm Required Search Term
 *  @param sort       Sorting Type - Defaults : Sort By Rating
 *  @param success    Success Block with the Request
 *  @param failure    Failure block with the NSError
 */
- (void) createSearchRequestWithTerm:(NSString*)searchTerm andSortBy:(NSString*)sort successHandler:(void (^)(NSURLRequest *oAuthRequest))success failureHandler:(void (^) (NSError *error))failure {
    
    NSDictionary *params = [self generateSearchParameters:self.searchTerm sortBy:self.sortType];
    
    [self createOAuthRequest:kSearchPath parameters:params successHandler:^(NSURLRequest *oAuthRequest) {
        success(oAuthRequest);
    } failureHandler:^(NSError *error) {
        failure(error);
    }];
    
}

/**
 *  Create a business details request
 *
 *  @param businessID business Id to search qith
 *  @param success    Success block with request
 *  @param failure    failure block with error
 */
- (void) createBusinessRequestWithBusinessId:(NSString*)businessID successHandler:(void (^)(NSURLRequest *oAuthRequest))success failureHandler:(void (^) (NSError *error))failure  {
    NSDictionary *params = [self generateBusinessParams:businessID];
    
    NSString *businessPath = [NSString stringWithFormat:@"%@%@", kBusinessPath, businessID];

    [self createOAuthRequest:businessPath parameters:params successHandler:^(NSURLRequest *oAuthRequest) {
        success(oAuthRequest);
    } failureHandler:^(NSError *error) {
        failure(error);
    }];
}

/**
 *  Private Method to create an OAuthRequest
 *
 *  @param path    Search Path
 *  @param params  Required Search Params
 *  @param success Success Block with Request
 *  @param failure Failure Block with NSError
 */
- (void) createOAuthRequest:(NSString*)path parameters:(NSDictionary*)params successHandler:(void (^)(NSURLRequest *oAuthRequest))success failureHandler:(void (^) (NSError *error))failure {
    
    if ([kConsumerKey length] == 0 || [kConsumerSecret length] == 0 || [kToken length] == 0 || [kTokenSecret length] == 0) {
        NSError *error = [[NSError alloc] initWithDomain:@"OAuthErrorDomain" code:1000 userInfo:nil];
        failure (error);
    }

    NSURLRequest * request = [TDOAuth URLRequestForPath:path
                                          GETParameters:params
                                                 scheme:@"https"
                                                   host:kAPIHost
                                            consumerKey:kConsumerKey
                                         consumerSecret:kConsumerSecret
                                            accessToken:kToken
                                            tokenSecret:kTokenSecret];
    if (request == nil) {
        NSError *error = [[NSError alloc] initWithDomain:@"OAuthErrorDomain" code:1000 userInfo:nil];
        failure (error);
    } else {
        NSLog(@"URL is %@", request.URL);
        success (request);
    }
    
}

#pragma mark -
#pragma mark - GET BUSINESS DETAILS

- (void) getBusinessDeatailsForID:(NSString*)businessID completionHandler:(void (^)(NSDictionary *businessJSON, NSError *error))completionHandler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul), ^{
        NSURLSession *session = [NSURLSession sharedSession];
        [self createBusinessRequestWithBusinessId:businessID successHandler:^(NSURLRequest *oAuthRequest) {
            NSURLSessionDataTask *task = [session dataTaskWithRequest:oAuthRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if (!error && httpResponse.statusCode == 200) {
                    NSDictionary *businessResponseJSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionHandler(businessResponseJSON, error);
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionHandler(nil, error);
                    });
                }
            }];
            [task resume];
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(nil, error);
            });
        }];
    });
}

#pragma mark -
#pragma mark - GET A LIST OF RESTAURANTS

- (void) getListOfRestaurantsWithCompletionHandler:(void (^)(BOOL finished))completionHandler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul), ^{
        NSURLSession *session = [NSURLSession sharedSession];
        [self createSearchRequestWithTerm:self.searchTerm andSortBy:@"0" successHandler:^(NSURLRequest *oAuthRequest) {
            NSURLSessionDataTask *task = [session dataTaskWithRequest:oAuthRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                if (!error && httpResponse.statusCode == 200) {
                    NSDictionary *searchResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                    NSArray *businessArray = [searchResponse objectForKey:@"businesses"];
                    NSMutableArray *tempBizArray = [NSMutableArray array];
                    for (NSDictionary* businessDict in businessArray) {
                        Business * bizObject = [[Business alloc] initWithDictionary:businessDict];
                        [tempBizArray addObject:bizObject];
                    }
                    self.businesses = [NSArray arrayWithArray:tempBizArray];
                    [tempBizArray removeAllObjects];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionHandler(YES);
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completionHandler(NO);
                    });
                }
            }];
            [task resume];
            
        } failureHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(NO);
            });
        }];
    });
}
@end
