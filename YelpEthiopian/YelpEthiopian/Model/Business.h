//
//  Business.h
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Business : NSObject

@property (nonatomic, strong) NSString *businessID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *ratingImageURL;
@property (nonatomic, strong) NSString *businessImageURL;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *snippetText;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *neighbourhood;
@property (nonatomic, strong) NSString *stateCode;
@property (nonatomic, strong) NSString *countryCode;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *city;


- (id) initWithDictionary:(NSDictionary*)businessDictionary;

@end
