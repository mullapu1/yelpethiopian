//
//  Business.m
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import "Business.h"
@implementation Business

- (id) initWithDictionary:(NSDictionary *)businessDictionary {
    self.name = [businessDictionary objectForKey:@"name"];
    self.businessID = [businessDictionary objectForKey:@"id"];
    self.rating = [businessDictionary objectForKey:@"rating"];
    self.ratingImageURL = [businessDictionary objectForKey:@"rating_img_url_large"];
    self.businessImageURL = [businessDictionary objectForKey:@"image_url"];
    self.phone = [businessDictionary objectForKey:@"display_phone"];
    self.snippetText = [businessDictionary objectForKey:@"snippet_text"];
    
    NSDictionary *location = [businessDictionary objectForKey:@"location"];
    self.address = ((NSArray*)[location objectForKey:@"address"]).firstObject;
    self.neighbourhood = ((NSArray*)[location objectForKey:@"neighborhoods"]).firstObject;
    self.stateCode = [location objectForKey:@"state_code"];
    self.countryCode = [location objectForKey:@"country_code"];
    self.city = [location objectForKey:@"city"];
    self.postalCode = [location objectForKey:@"postal_code"];
    
    return self;
}

@end
