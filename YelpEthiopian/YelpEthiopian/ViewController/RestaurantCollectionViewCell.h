//
//  RestaurantCollectionViewCell.h
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RestaurantCellViewDelegate;
@interface RestaurantCollectionViewCell : UICollectionViewCell
{
    NSInteger item ;
}
@property (assign, nonatomic) __unsafe_unretained id<RestaurantCellViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;
@property (weak, nonatomic) IBOutlet UIImageView *ratingsImage;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *getMoreInfo;
@property (weak, nonatomic) IBOutlet UILabel *snippetText;

- (void) setIndexPathRow:(NSInteger)indexPathItem;
- (IBAction)getInfoDidGetTriggered:(id)sender;

@end


@protocol RestaurantCellViewDelegate <NSObject>

@optional
- (void) getInfoDidGetTriggered:(NSIndexPath*)indexPath;

@end