//
//  RestaurantCollectionViewCell.m
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import "RestaurantCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>


@implementation RestaurantCollectionViewCell

- (void) awakeFromNib {
    [self setRedAppearanceForButton];
}

- (void) setRedAppearanceForButton {
    [self.getMoreInfo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.getMoreInfo.layer setCornerRadius:5.0];
    [self.getMoreInfo.layer setBorderWidth:1.0];
    
    UIColor *borderColor = [UIColor colorWithRed:189.0/255 green:0 blue:0 alpha:1.0];
    UIColor *bgColor = [UIColor colorWithRed:230.0/255 green:30.0/255 blue:5.0/255 alpha:1.0];
    [self.getMoreInfo setBackgroundColor:bgColor];
    self.getMoreInfo.layer.borderColor = borderColor.CGColor;
}

-(void) setIndexPathRow:(NSInteger)indexPathItem {
    item = indexPathItem;
}

- (IBAction)getInfoDidGetTriggered:(id)sender {
    if ([self.delegate respondsToSelector:@selector(getInfoDidGetTriggered:)]) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:item inSection:0];
        [self.delegate getInfoDidGetTriggered:indexPath];
    }
}
@end
