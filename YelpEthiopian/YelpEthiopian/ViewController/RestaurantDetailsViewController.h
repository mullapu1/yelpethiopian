//
//  RestaurantDetailsViewController.h
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-03-01.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import "BaseViewController.h"
@class Business;
@interface RestaurantDetailsViewController : BaseViewController <UIWebViewDelegate>

@property (nonatomic, strong) Business *bizObject;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *neighbourhood;
@property (weak, nonatomic) IBOutlet UILabel *cityAndOtherInfo;

@property (weak, nonatomic) IBOutlet UIImageView *ratingsView;
@property (weak, nonatomic) IBOutlet UILabel *latestReview;
@property (weak, nonatomic) IBOutlet UILabel * reviewerName;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIWebView *webview;
@end
