//
//  RestaurantDetailsViewController.m
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-03-01.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import "RestaurantDetailsViewController.h"
#import "Business.h"
#import "DataManager.h"
#import "UIKit+AFNetworking.h"
@interface RestaurantDetailsViewController ()

@end

@implementation RestaurantDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.bizObject.name;
  
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
    self.address.text = self.bizObject.address;
    self.neighbourhood.text = [NSString stringWithFormat:@"Neighbourhood: %@", self.bizObject.neighbourhood];
    self.cityAndOtherInfo.text = [NSString stringWithFormat:@"%@ %@, %@", self.bizObject.city, self.bizObject.stateCode, self.bizObject.postalCode];
    
    [self loadBizDetails];
    
    NSString *photosURLString = [NSString stringWithFormat:@"https://www.yelp.ca/biz_photos/%@", self.bizObject.businessID];
    NSURL *photosURL = [NSURL URLWithString:photosURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:photosURL];
    self.webview.delegate = self;
    [self.webview loadRequest:request];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  Downloads business detail info
 */
- (void) loadBizDetails {
    [self.activityIndicator startAnimating];
    self.latestReview.text = @"";
    self.reviewerName.text = @"";
    
    [[DataManager sharedManager] getBusinessDeatailsForID:self.bizObject.businessID completionHandler:^(NSDictionary *businessJSON, NSError *error) {
       
            NSDictionary *latestReview = ((NSArray*)[businessJSON objectForKey:@"reviews"]).firstObject;
            NSString *ratingURLString = [latestReview objectForKey:@"rating_image_large_url"];
            self.latestReview.text = [latestReview objectForKey:@"excerpt"];
            self.reviewerName.text = [NSString stringWithFormat:@"By: %@", [((NSDictionary*)[latestReview objectForKey:@"user"]) objectForKey:@"name"]];
            
            [self.ratingsView setImageWithURL:[NSURL URLWithString:ratingURLString]];
            NSString *userImageURLString = [((NSDictionary*)[latestReview objectForKey:@"user"]) objectForKey:@"image_url"];
            NSLog(@"User Image URL ---- %@", userImageURLString);
            [self.userImage setImageWithURL:[NSURL URLWithString:userImageURLString]];
            
            [self.activityIndicator stopAnimating];
    }];
}

#pragma mark -
#pragma mark - Webview delegate

- (void) webViewDidStartLoad:(UIWebView *)webView {
    [self.activityIndicator startAnimating];
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.activityIndicator stopAnimating];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
