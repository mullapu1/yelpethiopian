//
//  RestaurantsListViewController.h
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RestaurantCollectionViewCell.h"

@interface RestaurantsListViewController : BaseViewController <UICollectionViewDataSource, UICollectionViewDelegate, RestaurantCellViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *restaurantCollection;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@end
