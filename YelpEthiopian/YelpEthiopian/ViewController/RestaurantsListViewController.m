//
//  RestaurantsListViewController.m
//  YelpEthiopian
//
//  Created by Harish Mullapudi on 2016-02-29.
//  Copyright © 2016 Harish Mullapudi. All rights reserved.
//

#import "RestaurantsListViewController.h"
#import "DataManager.h"
#import "Business.h"
#import "AFNetworking.h"
#import "UIKit+AFNetworking.h"
#import "RestaurantDetailsViewController.h"

static NSString* const kCellIdentifier = @"restaurantCell";
@interface RestaurantsListViewController ()

@end

@implementation RestaurantsListViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem.backBarButtonItem setTitle:@" "];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateList:) name:@"sortValueUpdated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateList:) name:@"searchTermUpdated" object:nil];
    
    [self loadCollectionViewData];
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
   
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self.restaurantCollection reloadData];
    }];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -
#pragma mark - NSNotificationCenter

- (void) shouldUpdateList:(NSNotification*)notification {
    [self loadCollectionViewData];
}

#pragma mark -
#pragma mark - Load Collection View Data

- (void) loadCollectionViewData {
    __weak RestaurantsListViewController *weakSelf = self;
    [self.activityIndicatorView startAnimating];
    [[DataManager sharedManager] getListOfRestaurantsWithCompletionHandler:^(BOOL finished) {
        [self.activityIndicatorView stopAnimating];
        if (finished) {
            [weakSelf.restaurantCollection reloadData];
        } else {
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unable to load the list" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
            [ac addAction:okAction];
            [self presentViewController:ac animated:YES completion:nil];
        }
    }];
}

#pragma mark -
#pragma mark - Collection View Data Source

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[DataManager sharedManager] businesses].count;
}

- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RestaurantCollectionViewCell *cell = (RestaurantCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    [cell setIndexPathRow:indexPath.item];
    [cell setDelegate:self];
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)collectionView.collectionViewLayout;
    
    CGRect frame = cell.frame;
    frame.size.width = collectionView.bounds.size.width - collectionView.contentInset.left - collectionView.contentInset.right - layout.sectionInset.left - layout.sectionInset.right;
    frame.origin.x = layout.sectionInset.left;
    [cell setFrame:frame];
    
    Business *bizObject = [[[DataManager sharedManager] businesses] objectAtIndex:indexPath.item];
    [cell.ratingsImage setImageWithURL:[NSURL URLWithString:bizObject.ratingImageURL]];
    [cell.restaurantImage setImageWithURL:[NSURL URLWithString:bizObject.businessImageURL]];
    cell.name.text = bizObject.name;
    cell.phoneNumber.text = bizObject.phone;
    cell.snippetText.text = bizObject.snippetText;
    
    return cell;
}

#pragma mark -
#pragma mark - RestaurantCellViewDelegate

- (void) getInfoDidGetTriggered:(NSIndexPath *)indexPath {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"RestaurantDetails" bundle:nil];
    RestaurantDetailsViewController *vc = (RestaurantDetailsViewController*)[sb instantiateInitialViewController];
    vc.bizObject = [[[DataManager sharedManager] businesses] objectAtIndex:indexPath.item];
    
    [self.navigationController showViewController:vc sender:self];
}

@end
